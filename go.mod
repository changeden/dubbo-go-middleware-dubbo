module gitee.com/changeden/dubbo-go-middleware-dubbo

go 1.16

require (
	dubbo.apache.org/dubbo-go/v3 v3.0.1
	gitee.com/changeden/dubbo-go-starter v0.1.4
)
